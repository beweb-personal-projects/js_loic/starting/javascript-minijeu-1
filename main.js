let arr_1 = []; // Array "Entrées"
let arr_2 = []; // Array "Plat Principal"
let arr_3 = []; // Array "Dessert"

const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

console.clear(); // Clear console avant d'initializer le petit jeu

// Premiere question de verification, T'as le choix de créer une nouvelle carte, quitter ou "cmd" inconnu
function questionIniciale() {
    rl.question("Crée = y // Quitter // n : ", function(answer) {
        if (answer === "y"){
            questionTerminal1();
        } else if (answer === "n") {
            console.log("T'as quitter le programme!")
            process.exit();
        } else {
            console.log("Sortie forcée, commande inconnue !")
            process.exit();
        }
    });  
}

// Si on choisi de créer une nouvelle carte la fonction "questionIniciale()" va inicializer la premiere partie du jeu
questionIniciale();

// Premier partie
function questionTerminal1() {
    rl.question("Entrée: ", function(answer) {
        // Si on a 4 données dans l'array ça va passer à la deuxieme partie, si non, ça va repeter.
        // Si le if est true, on va voir aussi le tableau qu'on vient d'ecrire.
        if (arr_1.length === 4){
            UIshow(true);
            questionTerminal2();
        } else {
            // UIconstruct va envoyer les données, ces données vont etres ajouter dans l'array.
            UIcontruct1(answer);
            questionTerminal1();
        }
    });  
}

// Deuxieme partie
function questionTerminal2() {
    rl.question("Plat Principal: ", function(answer) {
        // Si on a 4 données dans l'array ça va passer à la troisieme partie, si non, ça va repeter.
        // Si le if est true, on va voir les autres array (Entrées, plat principal).
        if (arr_2.length === 4){
            UIshow(true);
            questionTerminal3();
        } else {
            // UIconstruct va envoyer les données, ces données vont etres ajouter dans l'array.
            UIcontruct2(answer);
            questionTerminal2();
        }
    });  
}

// Troisieme partie
function questionTerminal3() {
    rl.question("Desserts: ", function(answer) {
        // Si on a 4 données dans l'array ça va passer à la troisieme partie, si non, ça va repeter.
        // Si le if est true, on va voir tout les array (Entrées, plat principal, desserts).
        if (arr_3.length === 4){
            UIshow(true);
        } else {
            // UIconstruct va envoyer les données, ces données vont etres ajouter dans l'array.
            UIcontruct3(answer);
            questionTerminal3();
        }
    });  
}

// ça va ajouter les données de la premier partie ici
const UIcontruct1 = (menuLine) => {
    arr_1.push(menuLine)
}

// ça va ajouter les données de la deuxieme partie ici
const UIcontruct2 = (menuLine) => {
    arr_2.push(menuLine)
}

// ça va ajouter les données de la troisieme partie ici
const UIcontruct3 = (menuLine) => {
    arr_3.push(menuLine)
}

// Verification de la taille max des mots que j'ai envoyé. ça va return le max valeur, comme ça le UI va etre dynamique.
const UIlen = (max_length) => {

    // Check max lengh arr_1
    for (var i = 0; i < arr_1.length; i++) {
        var obj = arr_1[i]   
        if (max_length < obj.length) {
            max_length = obj.length;
        }
    }

    // Check max lengh arr_2
    for (var i = 0; i < arr_2.length; i++) {
        var obj = arr_2[i]   
        if (max_length < obj.length) {
            max_length = obj.length;
        }
    }

    // Check max lengh arr_3
    for (var i = 0; i < arr_3.length; i++) {
        var obj = arr_3[i]   
        if (max_length < obj.length) {
            max_length = obj.length;
        }
    }

    // return des valeur, j'ai ajouter 4, comme ça on donné une petite marge entre le text e ligne.
    return max_length += 4;
}

// UIshow va gerer la partie interface -> client
const UIshow = (state) => {
    if (state) {
        console.clear(); // Effacer la console à chaque fois UIshow est appellé.
        let ligne = "#"; // Type de ligne
        let max_length = 12; // J'ai mis un protection de min length, techniquemente c'est 12;
        let addspaces_length = 0; // Space entre le text et #

        // On a la fonction UIlen que retourne le max_lenght correct.
        max_length = UIlen(max_length);

        // Correction de lenght entre le #-------# 
        let ligne_separer = "#" + ("-".repeat(max_length).slice(0, -2)) + "#";

        console.clear(); // Effacer la console à chaque fois UIshow est appellé.
        console.log(ligne.repeat(max_length)); // Premier ligne de #
        console.log("# LA CARTE #"); 
        console.log(ligne_separer); 
        console.log(ligne_separer); 

        //ligne 1 et ligne 2 vont crées una ligne entre l'entrée, plat principal et dessert, ça va etre true si on a quelques chose dans l'array.

        let ligne1 = false;

        // Boucle que va printer la liste (entrée) - arr_1, ça va aussi "manipuler" les lignes du taubleau.
        for (var i = 0; i < arr_1.length; i++) {
            var obj = arr_1[i]   
            addspaces_length = max_length - obj.length;
            console.log("#" + obj + " ".repeat(addspaces_length).slice(0, -2) + "#");
            ligne1 = true;
        }

        if (ligne1) {
            console.log(ligne_separer);
        }

        let ligne2 = false;
        
        // Boucle que va printer la liste (Plat Principal) - arr_2, ça va aussi "manipuler" les lignes du taubleau.
        for (var i = 0; i < arr_2.length; i++) {
            var obj = arr_2[i]
            addspaces_length = max_length - obj.length;
            console.log("#" + obj + " ".repeat(addspaces_length).slice(0, -2) + "#");
            ligne2 = true;
        }
        
        if (ligne2) {
            console.log(ligne_separer);
        }

        // Boucle que va printer la liste (Dessert) - arr_3, ça va aussi "manipuler" les lignes du taubleau.
        for (var i = 0; i < arr_3.length; i++) {
            var obj = arr_3[i]   
            addspaces_length = max_length - obj.length;
            console.log("#" + obj + " ".repeat(addspaces_length).slice(0, -2) + "#");
        }

        console.log(ligne.repeat(max_length));
    }
}

